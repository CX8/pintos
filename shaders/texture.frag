#version 120

varying vec2 tex_coord;
uniform sampler2D texture;

void main(void) {
	
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	gl_FragColor = texture2D(texture, tex_coord);
	
}