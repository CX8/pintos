#version 120
varying vec3 normal;
varying vec2 tex_coord;
uniform sampler2D texture;

uniform bool use_tex;
 
void main()
{
    vec3 n, halfV, lightDir;
    float NdotL,NdotHV;
	
	vec4 color, diffuse, ambient;
 
    lightDir = vec3(gl_LightSource[0].position);
 
    /* Normalize interpolated normal */
    n = normalize(normal);
	
	if (use_tex) {
		diffuse = texture2D(texture, tex_coord) * gl_LightSource[0].diffuse;
		ambient = texture2D(texture, tex_coord) * gl_LightSource[0].ambient;
	} else {
	    /* Compute the diffuse, ambient and globalAmbient terms */
		diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;	
	    ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	    ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
	}

	
    /* Add ambient term */
    color = ambient;
	
    /* compute the dot product between normal and ldir */
    NdotL = max(dot(n,lightDir),0.0);
	if (NdotL > 0.0) {
        color += diffuse * NdotL;
        halfV = normalize(gl_LightSource[0].halfVector.xyz);
        NdotHV = max(dot(n,halfV),0.0);
        color += gl_FrontMaterial.specular *
                gl_LightSource[0].specular *
                pow(NdotHV, gl_FrontMaterial.shininess);
    }
 
	gl_FragColor = color;
 
}