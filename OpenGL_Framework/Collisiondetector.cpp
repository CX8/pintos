#include "Collisiondetector.h"
#include "Sphere.h"
#include "Wall.h"
#include <QMatrix4x4>
#include <QQuaternion>
#include <iostream>

CollisionDetector::CollisionDetector() {}

bool CollisionDetector::computeContinuosCollision(const Sphere& ball, const Wall &wall, const double& delta_t, std::pair<QVector2D, QVector2D>& hit_point) {
    /* Compute future position of the ball and check if it intersects the wall */
    /* Compute ball data in local coordiante of the plane */
    QVector2D ball_position_plane = QVector2D(ball.position.x(), ball.position.z());
    QVector2D ball_speed_plane = QVector2D(ball.speed.x(), ball.speed.z());
    QVector2D ball_speed_versor = ball_speed_plane.normalized();

    /* Compute data for the wall */
    QVector2D wall_directional = wall.end - wall.start;
    float wall_lenght = wall_directional.length();
    wall_directional.normalize();
    float wall_thickness = WALL_THICKNESS;
    /* Create directional vector perpendicular to direction */
    QQuaternion rotation_y = QQuaternion(-M_PI / 2.0, QVector3D(0,1,0));
    QMatrix4x4 rotation_matrix = QMatrix4x4();
    rotation_matrix.rotate(rotation_y);

    QVector3D wall_directional_perp_3D = rotation_matrix * QVector3D(wall_directional.x(), 0, wall_directional.y());
    /* Vector in  plane space */
    QVector2D wall_directional_perp = QVector2D(wall_directional_perp_3D.x(), wall_directional_perp_3D.z());
    wall_directional_perp.normalize();

    /* Compute intersection with wall borders, point contains collision point and normal at that point*/
    std::vector<std::pair<QVector2D, QVector2D> > collision_points;

    /* Wall's walls are numbered as
     *    3
     * 4     2
     *    1
     */

    /* Ball line in homogenous */
    QVector3D position_h = QVector3D(ball_position_plane, 1.0);
    QVector3D future_position_h = QVector3D(ball_position_plane + ball_speed_versor, 1.0);
    QVector3D ball_line_h = QVector3D::crossProduct(position_h, future_position_h);

    /* Check intersection with wall 1 */
    /* Compute intersection point in homogenous cooridnates */
    QVector2D start_wall_1 = wall.start - wall_thickness / 2.0 * wall_directional_perp;
    QVector3D start_wall_1_h = QVector3D(start_wall_1, 1.0);
    QVector3D end_wall_1_h = QVector3D(start_wall_1 + wall_directional_perp * wall_thickness, 1.0);
    QVector3D wall_1_line_h = QVector3D::crossProduct(start_wall_1_h, end_wall_1_h);
    /* Compute intersection in homogenous coordinates */
    QVector3D intersection_wall_1_h = QVector3D::crossProduct(ball_line_h, wall_1_line_h);

    /* Check intersection with wall 2 */
    /* Compute intersection point in homogenous cooridnates */
    QVector2D start_wall_2 = wall.start + wall_thickness / 2.0 * wall_directional_perp;
    QVector3D start_wall_2_h = QVector3D(start_wall_2, 1.0);
    QVector3D end_wall_2_h = QVector3D(start_wall_2 + wall_directional * wall_lenght, 1.0);
    QVector3D wall_2_line_h = QVector3D::crossProduct(start_wall_2_h, end_wall_2_h);
    /* Compute intersection in homogenous coordinates */
    QVector3D intersection_wall_2_h = QVector3D::crossProduct(ball_line_h, wall_2_line_h);

    /* Check intersection with wall 3 */
    /* Compute intersection point in homogenous cooridnates */
    QVector2D start_wall_3 = start_wall_1 + wall_lenght * wall_directional;
    QVector3D start_wall_3_h = QVector3D(start_wall_3, 1.0);
    QVector3D end_wall_3_h = QVector3D(start_wall_3 + wall_directional_perp * wall_thickness, 1.0);
    QVector3D wall_3_line_h = QVector3D::crossProduct(start_wall_3_h, end_wall_3_h);
    /* Compute intersection in homogenous coordinates */
    QVector3D intersection_wall_3_h = QVector3D::crossProduct(ball_line_h, wall_3_line_h);

    /* Check intersection with wall 4 */
    /* Compute intersection point in homogenous cooridnates */
    QVector2D start_wall_4 = wall.start - wall_thickness / 2.0 * wall_directional_perp;
    QVector3D start_wall_4_h = QVector3D(start_wall_4, 1.0);
    QVector3D end_wall_4_h = QVector3D(start_wall_4 + wall_directional * wall_lenght, 1.0);
    QVector3D wall_4_line_h = QVector3D::crossProduct(start_wall_4_h, end_wall_4_h);
    /* Compute intersection in homogenous coordinates */
    QVector3D intersection_wall_4_h = QVector3D::crossProduct(ball_line_h, wall_4_line_h);

    /* Check if we have intersection with wall 1 */
    if (intersection_wall_1_h.z() != 0) {
        QVector2D intersection_wall_1 = QVector2D(intersection_wall_1_h.x() / intersection_wall_1_h.z(), intersection_wall_1_h.y() / intersection_wall_1_h.z());
        /* Check if intersection is on the wall or outside */
        double t;
        if (wall_directional_perp.x() != 0) {
            t = (intersection_wall_1.x() - start_wall_1.x()) / wall_directional_perp.x();
        } else {
            t = (intersection_wall_1.y() - start_wall_1.y()) / wall_directional_perp.y();
        }

        if (t >= 0 && t <= wall_thickness) {
            /* Check if point is after the ball */
            QVector2D ball_to_collision = intersection_wall_1 - ball_position_plane;
            if (QVector2D::dotProduct(ball_to_collision, ball_speed_versor) > 0 && ball_to_collision.length() <= (ball.speed * delta_t).length()) {
                collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_1, -wall_directional));
            }
        }
    }

    /* Check if we have intersection with wall 2 */
    if (intersection_wall_2_h.z() != 0) {
        QVector2D intersection_wall_2 = QVector2D(intersection_wall_2_h.x() / intersection_wall_2_h.z(), intersection_wall_2_h.y() / intersection_wall_2_h.z());
        /* Check if intersection is on the wall or outside */
        double t;
        if (wall_directional.x() != 0) {
            t = (intersection_wall_2.x() - start_wall_2.x()) / wall_directional.x();
        } else {
            t = (intersection_wall_2.y() - start_wall_2.y()) / wall_directional.y();
        }

        if (t >= 0 && t <= wall_lenght) {
            /* Check if point is after the ball */
            QVector2D ball_to_collision = intersection_wall_2 - ball_position_plane;
            if (QVector2D::dotProduct(ball_to_collision, ball_speed_versor) > 0 && ball_to_collision.length() <= (ball.speed * delta_t).length()) {
                collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_2, wall_directional_perp));
            }
        }
    }

    /* Check if we have intersection with wall 3 */
    if (intersection_wall_3_h.z() != 0) {
        QVector2D intersection_wall_3 = QVector2D(intersection_wall_3_h.x() / intersection_wall_3_h.z(), intersection_wall_3_h.y() / intersection_wall_3_h.z());
        /* Check if intersection is on the wall or outside */
        double t;
        if (wall_directional_perp.x() != 0) {
            t = (intersection_wall_3.x() - start_wall_3.x()) / wall_directional_perp.x();
        } else {
            t = (intersection_wall_3.y() - start_wall_3.y()) / wall_directional_perp.y();
        }

        if (t >= 0 && t <= wall_thickness) {
            /* Check if point is after the ball */
            QVector2D ball_to_collision = intersection_wall_3 - ball_position_plane;
            if (QVector2D::dotProduct(ball_to_collision, ball_speed_versor) > 0 && ball_to_collision.length() <= (ball.speed * delta_t).length()) {
                collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_3, wall_directional));
            }
        }
    }

    /* Check if we have intersection with wall 4 */
    if (intersection_wall_4_h.z() != 0) {
        QVector2D intersection_wall_4 = QVector2D(intersection_wall_4_h.x() / intersection_wall_4_h.z(), intersection_wall_4_h.y() / intersection_wall_4_h.z());
        /* Check if intersection is on the wall or outside */
        double t;
        if (wall_directional.x() != 0) {
            t = (intersection_wall_4.x() - start_wall_4.x()) / wall_directional.x();
        } else {
            t = (intersection_wall_4.y() - start_wall_4.y()) / wall_directional.y();
        }

        if (t >= 0 && t <= wall_lenght) {
            /* Check if point is after the ball */
            QVector2D ball_to_collision = intersection_wall_4 - ball_position_plane;
            if (QVector2D::dotProduct(ball_to_collision, ball_speed_versor) > 0 && ball_to_collision.length() <= (ball.speed * delta_t).length()) {
                collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_4, -wall_directional_perp));
            }
        }
    }

    if (collision_points.size() == 0) {
        return false;
    } else {
        /* Select nearest point to the ball */
        std::pair<QVector2D, QVector2D> min = collision_points[0];
        for (std::vector<std::pair<QVector2D, QVector2D> >::const_iterator it = collision_points.begin() + 1; it != collision_points.end(); ++it) {
            if ((it->first - ball.position).length() < (min.first - ball.position).length()) {
                min = *it;
            }
        }
        hit_point = min;
        return true;
    }
}

bool CollisionDetector::computeCollision(const Sphere& ball, const Wall& wall, std::pair<QVector2D, QVector2D>& hit_point) {
    /* Compute future position of the ball and check if it intersects the wall */
    /* Compute ball data in local coordiante of the plane */
    QVector2D ball_position_plane = QVector2D(ball.position.x(), ball.position.z());
    QVector2D ball_speed_plane = QVector2D(ball.speed.x(), ball.speed.z());
    QVector2D ball_speed_versor = ball_speed_plane.normalized();

    /* Compute data for the wall */
    QVector2D wall_directional = wall.end - wall.start;
    float wall_lenght = wall_directional.length();
    wall_directional.normalize();
    float wall_thickness = WALL_THICKNESS;
    /* Create directional vector perpendicular to direction */
    QMatrix4x4 rotation_matrix = QMatrix4x4();
    rotation_matrix.rotate(-90, 0,1,0);

    QVector3D wall_directional_perp_3D = rotation_matrix * QVector3D(wall_directional.x(), 0, wall_directional.y());
    /* Vector in  plane space */
    QVector2D wall_directional_perp = QVector2D(wall_directional_perp_3D.x(), wall_directional_perp_3D.z());
    wall_directional_perp.normalize();

    /* Compute intersection with wall borders, point contains collision point and normal at that point*/
    std::vector<std::pair<QVector2D, QVector2D> > collision_points;

    /* Check intersection with wall 1 */
    QVector2D start_wall_1 = wall.start - wall_thickness / 2.0 * wall_directional_perp;
    QVector2D end_wall_1 = start_wall_1 + wall_directional_perp * wall_thickness;
    /* Compute closest point on wall 1 */
    double distance_wall_1 = ball_position_plane.distanceToLine(start_wall_1, wall_directional_perp);

    /* Check intersection with wall 2 */
    QVector2D start_wall_2 = wall.start + wall_thickness / 2.0 * wall_directional_perp;
    QVector2D end_wall_2 = start_wall_2 + wall_directional * wall_lenght;
    /* Compute closest point on wall 1 */
    double distance_wall_2 = ball_position_plane.distanceToLine(start_wall_2, wall_directional);

    /* Check intersection with wall 3 */
    QVector2D start_wall_3 = start_wall_1 + wall_lenght * wall_directional;
    QVector2D end_wall_3 = start_wall_3 + wall_directional_perp * wall_thickness;
    /* Compute closest point on wall 1 */
    double distance_wall_3 = ball_position_plane.distanceToLine(start_wall_3, wall_directional_perp);

    /* Check intersection with wall 4 */
    QVector2D start_wall_4 = wall.start - wall_thickness / 2.0 * wall_directional_perp;
    QVector2D end_wall_4 = start_wall_4 + wall_directional * wall_lenght;
    /* Compute closest point on wall 1 */
    double distance_wall_4 = ball_position_plane.distanceToLine(start_wall_4, wall_directional);


    /* Check if we have intersection with wall 1 */
    if (distance_wall_1 <= ball.radius) {
        /* Check that the ball is in front of the wall */
        if (QVector2D::dotProduct(-wall_directional, ball_position_plane - start_wall_1) > 0.0) {
//            std::cout << "Ball is facing wall 1" <<std::endl;
            /* Compute intersection point */
            double t = QVector2D::dotProduct(ball_position_plane - start_wall_1, wall_directional_perp);
            QVector2D intersection_wall_1 = start_wall_1 + t * wall_directional_perp;
            /* Check if intersection is on the wall or outside */
            if (t >= -ball.radius && t <= wall_thickness + ball.radius) {
                if (t < 0) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - start_wall_1).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(start_wall_1, (-wall_directional - wall_directional_perp).normalized()));
                    }
                } else if (t > wall_thickness) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - end_wall_1).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(end_wall_1, (wall_directional_perp - wall_directional).normalized()));
                    }
                } else {
                    /* Point is on the wall */
                    collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_1, -wall_directional));
                }
            }
        }
    }

    /* Check if we have intersection with wall 2 */
    if (distance_wall_2 <= ball.radius) {
        /* Check that the ball is in front of the wall */
        if (QVector2D::dotProduct(wall_directional_perp, ball_position_plane - start_wall_2) > 0.0) {
//            std::cout << "Ball is facing wall 2" <<std::endl;
            /* Compute intersection point */
            double t = QVector2D::dotProduct(ball_position_plane - start_wall_2, wall_directional);
//            std::cout << "t: " << t << std::endl;
            QVector2D intersection_wall_2 = start_wall_2 + t * wall_directional;
            /* Check if intersection is on the wall or outside */
            if (t >= -ball.radius && t <= wall_lenght + ball.radius) {
//                std::cout << "Check collision 2" << std::endl;
                if (t < 0) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - start_wall_2).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(start_wall_2, (-wall_directional + wall_directional_perp).normalized()));
                    }
                } else if (t > wall_lenght) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - end_wall_2).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(end_wall_2, (wall_directional_perp + wall_directional).normalized()));
                    }
                } else {
//                    std::cout << "Point on wall 2" <<std::endl;
                    /* Point is on the wall */
                    collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_2, wall_directional_perp));
                }
            }
        }
//        QVector2D intersection_wall_2 = start_wall_2 + QVector2D::dotProduct(ball_position_plane - start_wall_2, wall_directional) * wall_directional;
//        /* Check if intersection is on the wall or outside */
//        double t;
//        if (wall_directional.x() != 0) {
//            t = (intersection_wall_2.x() - start_wall_2.x()) / wall_directional.x();
//        } else {
//            t = (intersection_wall_2.y() - start_wall_2.y()) / wall_directional.y();
//        }

//        double speed_on_normal_proj = QVector2D::dotProduct(ball_speed_versor, wall_directional_perp);
//        if (t >= -ball.radius && t <= wall_lenght + ball.radius && speed_on_normal_proj < 0) {
//            /* Check if point is in direction of the ball */
//            collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_2, wall_directional_perp));
//        }
    }

    /* Check if we have intersection with wall 3 */
    if (distance_wall_3 <= ball.radius) {
        /* Check that the ball is in front of the wall */
        if (QVector2D::dotProduct(wall_directional, ball_position_plane - start_wall_3) > 0.0) {
//            std::cout << "Ball is facing wall 3" <<std::endl;
            /* Compute intersection point */
            double t = QVector2D::dotProduct(ball_position_plane - start_wall_3, wall_directional_perp);
            QVector2D intersection_wall_3 = start_wall_3 + t * wall_directional_perp;
            /* Check if intersection is on the wall or outside */
            if (t >= -ball.radius && t <= wall_thickness + ball.radius) {
                if (t < 0) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - start_wall_3).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(start_wall_3, (wall_directional - wall_directional_perp).normalized()));
                    }
                } else if (t > wall_thickness) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - end_wall_3).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(end_wall_3, (wall_directional_perp + wall_directional).normalized()));
                    }
                } else {
                    /* Point is on the wall */
                    collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_3, wall_directional));
                }
            }
        }
//        QVector2D intersection_wall_3 = start_wall_3 + QVector2D::dotProduct(ball_position_plane - start_wall_3, wall_directional_perp) * wall_directional_perp;
//        /* Check if intersection is on the wall or outside */
//        double t;
//        if (wall_directional_perp.x() != 0) {
//            t = (intersection_wall_3.x() - start_wall_3.x()) / wall_directional_perp.x();
//        } else {
//            t = (intersection_wall_3.y() - start_wall_3.y()) / wall_directional_perp.y();
//        }

//        double speed_on_normal_proj = QVector2D::dotProduct(ball_speed_versor, wall_directional);

//        if (t >= -ball.radius && t <= wall_thickness + ball.radius && speed_on_normal_proj < 0) {
//            /* Check if point is in direction of the ball */
//            collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_3, wall_directional));
//        }
    }

    /* Check if we have intersection with wall 4 */
    if (distance_wall_4 <= ball.radius) {
        /* Check that the ball is in front of the wall */
        if (QVector2D::dotProduct(-wall_directional_perp, ball_position_plane - start_wall_4) > 0.0) {
//            std::cout << "Ball is facing wall 4" <<std::endl;
            /* Compute intersection point */
            double t = QVector2D::dotProduct(ball_position_plane - start_wall_4, wall_directional);
//            std::cout << "t: " << t << std::endl;
            QVector2D intersection_wall_4 = start_wall_4 + t * wall_directional;
            /* Check if intersection is on the wall or outside */
            if (t >= -ball.radius && t <= wall_lenght + ball.radius) {
//                std::cout << "Check collision 4" << std::endl;
                if (t < 0) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - start_wall_4).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(start_wall_4, (-wall_directional - wall_directional_perp).normalized()));
                    }
                } else if (t > wall_lenght) {
                    /* Check if we have a collision with the corner of the wall */
                    double corner_distance = (ball_position_plane - end_wall_4).length();
                    if (corner_distance <= ball.radius) {
                        collision_points.push_back(std::pair<QVector2D, QVector2D>(end_wall_4, (-wall_directional_perp + wall_directional).normalized()));
                    }
                } else {
//                    std::cout << "Point on wall 4" <<std::endl;
                    /* Point is on the wall */
                    collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_4, -wall_directional_perp));
                }
            }
        }
//        QVector2D intersection_wall_4 = start_wall_4 + QVector2D::dotProduct(ball_position_plane - start_wall_4, wall_directional) * wall_directional;
//        /* Check if intersection is on the wall or outside */
//        double t;
//        if (wall_directional.x() != 0) {
//            t = (intersection_wall_4.x() - start_wall_4.x()) / wall_directional.x();
//        } else {
//            t = (intersection_wall_4.y() - start_wall_4.y()) / wall_directional.y();
//        }

//        double speed_on_normal_proj = QVector2D::dotProduct(ball_speed_versor, -wall_directional_perp);

//        if (t >= -ball.radius && t <= wall_lenght + ball.radius && speed_on_normal_proj < 0) {
//            collision_points.push_back(std::pair<QVector2D, QVector2D>(intersection_wall_4, -wall_directional_perp));
//        }
    }

    if (collision_points.size() != 0) {
//        std::cout << "Collision points " << collision_points.size() << std::endl;
    }

    if (collision_points.size() == 0) {
        return false;
    } else if (collision_points.size() == 2) {
        /* Select point */
//        std::cout << "Two points found " <<std::endl;
//        std::cout << "Point one: " << collision_points[0].first.x() << " " << collision_points[0].first.y() << std::endl;
//        std::cout << "Point one normal: " << collision_points[0].second.x() << " " << collision_points[0].second.y() << std::endl;
//        std::cout << "Point two: " << collision_points[1].first.x() << " " << collision_points[1].first.y() << std::endl;
//        std::cout << "Point two normal: " << collision_points[1].second.x() << " " << collision_points[1].second.y() << std::endl;

        /* Check if we get two points that are the same */
        hit_point.first = collision_points[0].first;
        hit_point.second = collision_points[0].second;
        hit_point.second = hit_point.second + collision_points[1].second;
        hit_point.second.normalize();

        return true;
    } else {
        std::pair<QVector2D, QVector2D> min = collision_points[0];
        for (std::vector<std::pair<QVector2D, QVector2D> >::const_iterator it = collision_points.begin() + 1; it != collision_points.end(); ++it) {
            if ((it->first - ball.position).length() < (min.first - ball.position).length()) {
                min = *it;
            }
        }
        hit_point = min;
        return true;
    }


}
