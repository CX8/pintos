#include "Piecegraphic.h"
#include "Wall.h"
#include <QtOpenGL>
#include <iostream>

#define NUM_POINTS 40

PieceGraphic::PieceGraphic() {}

unsigned int PieceGraphic::generateListFill(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col) {
    GLuint index = glGenLists(1);

    /* Compute texture cooridinate of the bottom-left corner of the piece */
    double tex_step_u = 1 / static_cast<double>(num_col);
    double tex_step_v = 1 / static_cast<double>(num_row);
    double tex_u = c * tex_step_u;
    double tex_v = r * tex_step_v;

    glNewList(index, GL_COMPILE);
        glBegin(GL_TRIANGLES);

        //top of the piece
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(tex_u, tex_v + tex_step_v);
        glVertex3d(-0.5, 0, -0.5 );
        glTexCoord2d(tex_u, tex_v);
        glVertex3d(-0.5, 0, 0.5 );
        glTexCoord2d(tex_u + tex_step_u, tex_v);
        glVertex3d(0.5, 0, 0.5 );

        glTexCoord2d(tex_u + tex_step_u, tex_v + tex_step_v);
        glVertex3d(0.5, 0, -0.5 );
        glTexCoord2d(tex_u, tex_v + tex_step_v);
        glVertex3d(-1 * 0.5, 0, -1 * 0.5 );
        glTexCoord2d(tex_u + tex_step_u, tex_v);
        glVertex3d(0.5, 0, 0.5 );

        //top corner
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(0, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step_u, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );

        //right corner
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step_u, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5);
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );

        //bottom corner
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(tex_step_u, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5);
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );

        //left corner
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(tex_step_u, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step_u, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );

        //bottom of the piece
        glNormal3d(0.0, -1.0, 0.0);
        glTexCoord2d(tex_u + tex_step_u, tex_v + tex_step_v);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_u,tex_v + tex_step_v);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_u + tex_step_u,tex_v);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );

        glTexCoord2d(tex_u,tex_v +tex_step_v);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_u, tex_v);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(tex_u + tex_step_u,tex_v);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );

        glEnd();

    glEndList();
    return index;
}

unsigned int PieceGraphic::generateListHole(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col) {
    GLuint index = glGenLists(1);

    /* Generate points for the hole inside */
    std::vector<QVector2D> circle_points;
    double angle = 2 * M_PI / NUM_POINTS;
    double s_r = sinf(angle);
    double c_r = cosf(angle);

    double x_p, y_p;
    x_p = 0.35;
    y_p = 0;

    circle_points.push_back(QVector2D(x_p, y_p));

    for (int i = 0; i < NUM_POINTS-1; i++) {
        // Compute next point
        float x_n = x_p * c_r - y_p * s_r;
        float y_n = x_p * s_r + y_p * c_r;

        circle_points.push_back(QVector2D(x_n, y_n));

        x_p = x_n;
        y_p = y_n;
    }

    std::vector<QVector2D> square_points;

    /* Generate the points on the square around */
    for (unsigned int i = 0; i < NUM_POINTS; i++) {
        QVector2D direction = circle_points[i];
        double t_right, t_top, t_left, t_bot;
        double y_coord_right, y_coord_left, x_coord_top, x_coord_bot;

        /* Compute interesection with border to find corresponding point on the square around */
        /* First check intersection with border (0.5, y). Condition is that -0.5 <= y <= 0.5 */
        t_right = 0.5 / direction.x();
        y_coord_right = t_right * direction.y();
        /* Second check intersection with border (x, -0.5). Condition is that -0.5 <= x <= 0.5 */
        t_top = 0.5 / direction.y();
        x_coord_top = t_top * direction.x();
        /* Third check intersection with border (-0.5, y). Condition is that -0.5 <= y <= 0.5 */
        t_left = -0.5 / direction.x();
        y_coord_left = t_left * direction.y();
        /* Last check intersection with border (x, 0.5). Condition is that -0.5 <= x <= 0.5 */
        t_bot = -0.5 / direction.y();
        x_coord_bot = t_bot * direction.x();

        /* Check what is the point that we want */
        if (t_right > 0 && y_coord_right >= -0.5 && y_coord_right <= 0.5) {
            square_points.push_back(direction * t_right);
        } else if (t_top > 0 && x_coord_top >= -0.5 && x_coord_top < 0.5) {
            square_points.push_back(direction * t_top);
        } else if (t_left > 0 && y_coord_left >= -0.5 && y_coord_left <= 0.5) {
            square_points.push_back(direction * t_left);
        } else if (t_bot > 0 && x_coord_bot >= -0.5 && x_coord_bot <= 0.5) {
            square_points.push_back(direction * t_bot);
        } else {
            std::cout << "Problem computing square points" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

//    for (unsigned int i = 0; i < square_points.size(); i++) {
//        std::cout << square_points[i].x() << " " << square_points[i].y() << std::endl;
//    }

    /* Compute texture cooridinate of the bottom-left corner of the piece */
    double tex_step = 1 / static_cast<double>(num_col);
    double tex_u = c * tex_step;
    double tex_v = r * tex_step;

    glNewList(index, GL_COMPILE);

        /* Add top and bottom of the piece */
        glBegin(GL_TRIANGLE_STRIP);
        /* Top face */
        /* Normal is the same for all vertices */
        glNormal3d(0, 1, 0);
        for (unsigned int i = 0; i < NUM_POINTS; i++) {
            /* Compute local space according to the bottom left corner */
            QVector2D local_space_point_circle = circle_points[i] - QVector2D(-0.5, 0.5);
//            std::cout << local_space_point_circle.x() << " " << local_space_point_circle.y() << std::endl;
            QVector2D local_space_point_square = square_points[i] - QVector2D(-0.5, 0.5);
//            std::cout << local_space_point_square.x() << " " << local_space_point_square.y() << std::endl;
            /* Add two vertices, one for each list */
            glTexCoord2d(tex_u + fabs(local_space_point_circle.x()) * tex_step, tex_v + fabs(local_space_point_circle.y()) * tex_step);
            glVertex3d(circle_points[i].x(), 0, circle_points[i].y());
            glTexCoord2d(tex_u + fabs(local_space_point_square.x()) * tex_step, tex_v + fabs(local_space_point_square.y()) * tex_step);
            glVertex3d(square_points[i].x(), 0, square_points[i].y());
        }
        /* Add again first two to complete the circle */
        QVector2D local_space_point_circle = circle_points[0] - QVector2D(-0.5, 0.5);
        QVector2D local_space_point_square = square_points[0] - QVector2D(-0.5, 0.5);
        glTexCoord2d(tex_u + fabs(local_space_point_circle.x()) * tex_step, tex_v + fabs(local_space_point_circle.y()) * tex_step);
        glVertex3d(circle_points[0].x(), 0, circle_points[0].y());
        glTexCoord2d(tex_u + fabs(local_space_point_square.x()) * tex_step, tex_v + fabs(local_space_point_square.y()) * tex_step);
        glVertex3d(square_points[0].x(), 0, square_points[0].y());

        glEnd();

        glBegin(GL_TRIANGLE_STRIP);
        /* Bottom face */
        /* Normal is the same for all vertices */
        glNormal3d(0, -1, 0);
        for (unsigned int i = 0; i < NUM_POINTS; i++) {
            /* Compute local space according to the bottom left corner */
            QVector2D local_space_point_circle = circle_points[i] - QVector2D(-0.5, 0.5);
            QVector2D local_space_point_square = square_points[i] - QVector2D(-0.5, 0.5);
            /* Add two vertices, one for each list */
            glTexCoord2d(tex_u + fabs(local_space_point_circle.x()) * tex_step, tex_v + fabs(local_space_point_circle.y()) * tex_step);
            glVertex3d(circle_points[i].x(), -WALL_THICKNESS, circle_points[i].y());
            glTexCoord2d(tex_u + fabs(local_space_point_square.x()) * tex_step, tex_v + fabs(local_space_point_square.y()) * tex_step);
            glVertex3d(square_points[i].x(), -WALL_THICKNESS, square_points[i].y());
        }
        /* Add again first two to complete the circle */
        local_space_point_circle = circle_points[0] - QVector2D(-0.5, 0.5);
        local_space_point_square = square_points[0] - QVector2D(-0.5, 0.5);
        glTexCoord2d(tex_u + fabs(local_space_point_circle.x()) * tex_step, tex_v + fabs(local_space_point_circle.y()) * tex_step);
        glVertex3d(circle_points[0].x(), -WALL_THICKNESS, circle_points[0].y());
        glTexCoord2d(tex_u + fabs(local_space_point_square.x()) * tex_step, tex_v + fabs(local_space_point_square.y()) * tex_step);
        glVertex3d(square_points[0].x(), -WALL_THICKNESS, square_points[0].y());

        glEnd();

        /* Add intern of the hole */
        /* Compute texutre step according to number of points */
        double tex_step_intern = 1.0 / NUM_POINTS;
        glBegin(GL_TRIANGLES);
            for (unsigned int i = 0; i < NUM_POINTS; i++) {
                /* Compute normal fo the internal face */
                QVector3D p1 = QVector3D(circle_points[i].x(), 0, circle_points[i].y());
                QVector3D p2;
                if (i == NUM_POINTS - 1) {
                    p2 = QVector3D(circle_points[0].x(), 0, circle_points[0].y());
                } else {
                    p2 = QVector3D(circle_points[i+1].x(), 0, circle_points[i+1].y());
                }
                QVector3D p3 = QVector3D(circle_points[i].x(), -WALL_THICKNESS, circle_points[i].y());
                /* Compute the normal */
                QVector3D normal = QVector3D::crossProduct(p2 - p1, p3 - p1);
                normal.normalize();
                /* Add the two triangle for the internal faces */
                glNormal3d(normal.x(), normal.y(), normal.z());
                glTexCoord2d(tex_step_intern * i, WALL_THICKNESS / 10);
                glVertex3d(p1.x(), p1.y(), p1.z());
                glTexCoord2d(tex_step_intern * (i + 1), WALL_THICKNESS / 10);
                glVertex3d(p2.x(), p2.y(), p2.z());
                glTexCoord2d(tex_step_intern * i, 0);
                glVertex3d(p3.x(), p3.y(), p3.z());

                QVector3D p4 = p2;
                p4.setY(-WALL_THICKNESS);

                glTexCoord2d(tex_step_intern * i, 0);
                glVertex3d(p3.x(), p3.y(), p3.z());
                glTexCoord2d(tex_step_intern * (i + 1), WALL_THICKNESS / 10);
                glVertex3d(p2.x(), p2.y(), p2.z());
                glTexCoord2d(tex_step_intern * (i + 1), 0);
                glVertex3d(p4.x(), p4.y(), p4.z());
            }

        glEnd();

        /* Add lateral faces */
        glBegin(GL_TRIANGLES);
        //top corner
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(tex_step, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(1, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );

        //right corner
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(tex_step, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5);
        glTexCoord2d(tex_step, 0);
        glVertex3d(0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );

        //bottom corner
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(tex_step, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(tex_step, WALL_THICKNESS / 10.0);
        glVertex3d(0.5, 0, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5);
        glTexCoord2d(tex_step, 0);
        glVertex3d(0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );

        //left corner
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(tex_step, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(tex_step, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );

        glTexCoord2d(0, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, -0.5 );
        glTexCoord2d(tex_step, 0);
        glVertex3d(-0.5, -WALL_THICKNESS, 0.5 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-0.5, 0, -0.5 );

        glEnd();

    glEndList();
    return index;
}
