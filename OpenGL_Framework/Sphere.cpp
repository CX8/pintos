#include "Sphere.h"
#include "Spheregraphic.h"
#include <QtOpenGL>
#include <iostream>

Sphere::Sphere()
    : mass(1.0), radius(0.3), position(), speed(), acceleration() {
    SphereGraphic sg(40, 40);
    sphere_list_id = sg.generateList();
}

Sphere::Sphere(const double& m, const double& r, const QVector3D& p, const QVector3D& s, const QVector3D& a)
    : mass(m), radius(r), position(p), speed(s), acceleration(a) {
    SphereGraphic sg(40, 40);
    sphere_list_id = sg.generateList();
}

Sphere::~Sphere() {}

bool Sphere::draw(QGLShaderProgram *program, const bool is_falling) {
    glPushMatrix();
    program->setUniformValue("use_tex", false);
    /* Set material properties */
    GLfloat mat_ambient[] = { 0.25, 0.25, 0.25, 1.0 };
    GLfloat mat_diffuse[] = { 0.65, 0.65, 0.65, 1.0 };
    GLfloat mat_specular[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat shininess = 51.0;

    glMaterialfv( GL_FRONT, GL_AMBIENT, mat_ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
    glMaterialf ( GL_FRONT, GL_SHININESS, shininess );
    if (is_falling) {
        position.setY(position.y() - 0.05);
    }
    glTranslated(position.x(), position.y(), position.z());
    glScaled(radius, radius, radius);
    glCallList(sphere_list_id);

    /* Set material back to 0s */
    mat_ambient[0] = mat_ambient[1] = mat_ambient[2] = 0.0;
    mat_diffuse[0] = mat_diffuse[1] = mat_diffuse[2] = 0.0;
    mat_specular[0] = mat_specular[1] = mat_specular[2] = 0.0;
    shininess = 0.0;

    glMaterialfv( GL_FRONT, GL_AMBIENT, mat_ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
    glMaterialf ( GL_FRONT, GL_SHININESS, shininess );

    glPopMatrix();

    bool end_animation;
    position.y() < -2 ? end_animation = true : end_animation = false;

    return end_animation;
}
