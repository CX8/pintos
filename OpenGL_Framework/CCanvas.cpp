#include "CCanvas.h"
#include "Base.h"
#include "Spheregraphic.h"
#include "Boardgraphic.h"
#include "Shader.h"
#include "Texture.h"

#include <QMatrix4x4>

using namespace std;

//-----------------------------------------------------------------------------

void CCanvas::initializeGL()
{
    glEnable(GL_TEXTURE_2D);
    b = p.parsePintosFile("test.pintos", ball);
    ball_copy = ball;
    /* Init shader program */
    program_directional = createProgram(this, QString("shaders/vertex_dir.vertex"), QString("shaders/fragment_dir.frag"));

    /* Load texture */
    wood_tex = loadTexture("textures/board.bmp");
    dark_wood = loadTexture("textures/dark_wood.jpg");
    table_text = loadTexture("textures/tableTexture.jpg");
    knob_text = loadTexture("textures/knob.bmp");

    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);			        // Black Background
    glClearDepth(1.0f);									// Depth Buffer Setup
    glEnable(GL_DEPTH_TEST);                              // Enables Depth Testing
    glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

    GLfloat ambient[] = { 0.3, 0.3, 0.3, 1.0 };
    GLfloat diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
    GLfloat specular[] = { 0.5, 0.5, 0.5, 1.0 };
    GLfloat position[] = { 0.0, 1.0, 0.0, 0.0 };

    glLightfv( GL_LIGHT0, GL_AMBIENT, ambient );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular );
    glLightfv( GL_LIGHT0, GL_POSITION, position );
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );
};

//-----------------------------------------------------------------------------


void  CCanvas::glPerspective(
        const GLdouble fovy,
        const GLdouble aspect,
        const GLdouble zNear,
        const GLdouble zFar)
{
    const GLdouble d=1.0/tan(fovy/360.0*PI);
    const GLdouble delta=zNear-zFar;

    GLdouble *mat=new GLdouble[16];

    mat[0]=d/aspect;
    mat[1]=0;
    mat[2]=0;
    mat[3]=0;

    mat[4]=0;
    mat[5]=d;
    mat[6]=0;
    mat[7]=0;

    mat[8]=0;
    mat[9]=0;
    mat[10]=(zNear+zFar)/delta;
    mat[11]=-1;

    mat[12]=0;
    mat[13]=0;
    mat[14]=2*zNear*zFar/delta;
    mat[15]=0;


    glMultMatrixd(mat);

    delete[] mat;
}


void CCanvas::  lookAt(const GLdouble eyex,
                       const GLdouble eyey,
                       const GLdouble eyez,
                       const GLdouble centerx,
                       const GLdouble centery,
                       const GLdouble centerz,
                       const GLdouble upx,
                       const GLdouble upy,
                       const GLdouble upz)
{

    QMatrix4x4 look_at_matrix;

    look_at_matrix.lookAt(QVector3D(eyex, eyey, eyez), QVector3D(centerx, centery, centerz), QVector3D(upx, upy, upz));

    glMultMatrixf(look_at_matrix.data());
}

void CCanvas::resizeGL(int width, int height)
{
    // set up the window-to-viewport transformation
    glViewport( 0,0, width,height );

    // vertical camera opening angle

    // aspect ratio
    double gamma;
    if (height > 0)
        gamma = width/(double)height;
    else
        gamma = width;

    // front and back clipping plane at
    double n = -1.0;
    double f = -100.0;

    // frustum corners
    double t = -tan(beta*3.14159/360.0) * n;
    double b = -t;
    double r = gamma * t;
    double l = -r;

    // set projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum( l,r , b,t , -n,-f );

    // alternatively, directly from alpha and gamma
    //  glPerspective( beta, gamma, -n, -f );
}

//-----------------------------------------------------------------------------

void CCanvas::paintGL()
{

    // clear screen and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    // set model-view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    double y_camera = sin(phi) * camera_distance;
    if(y_camera <= -1 * (std::max(b->getWidth(), b->getHeight()) / 3.0 + WALL_THICKNESS)){
        y_camera = -1 * (std::max(b->getWidth(), b->getHeight()) / 3.0 + WALL_THICKNESS);
    }

    sphere_integrator.computeStep(ball, *b, 0.05);
    if (!end_game)
        end_game = sphere_integrator.checkEndGame(ball, *b);

    switch (view) {
    case FOLLOW_BALL:
        lookAt( ball.position.x() + sin(tau) * camera_distance * cos(phi), y_camera,ball.position.z() + cos(tau) * camera_distance * cos(phi),  ball.position.x(), ball.position.y(), ball.position.z(),  0,1,0 );
        break;
    case BALL:
        if(fabs(ball.speed.x()) <= 0.000001 && fabs(ball.speed.z()) <= 0.000001){
            lookAt( ball.position.x(), ball.position.y() + 0.3,ball.position.z(), ball.position.x(), ball.position.y() + 0.4, ball.position.z(),  0,1,0 );
        }
        else{
            lookAt( ball.position.x(), ball.position.y() + 0.3,ball.position.z(),  ball.position.x() + ball.speed.x(), ball.position.y() + 0.3, ball.position.z()+ ball.speed.z(),  0,1,0 );
        }
        break;
    default:// = TOP_BOARD
        lookAt( sin(tau) * camera_distance * cos(phi), y_camera,cos(tau) * camera_distance * cos(phi),  0,0,0,  0,1,0 );     // camera position , "look at" point , view-up vector
        break;
    }

    glScaled (1, 1, 1);
    if(!program_directional->bind()) {
        std::cout << "Error binding program" << std::endl;
        exit(EXIT_FAILURE);
    }

    program_directional->setUniformValue("texture", 0);

    /* Draw board */
    b->drawBox(program_directional, dark_wood);
    bool end_animation = b->draw(program_directional, ball, wood_tex, wood_tex, end_game);
    b->drawTable(program_directional, table_text); //pass another texture.
    b->drawKnobs(program_directional, knob_text);

    /* Unbind shader program */
    program_directional->release();

    if (end_animation) {
        ball = ball_copy;
        end_game = false;
        b->reset_board();
    }
}

void CCanvas::keyPressEvent(QKeyEvent *event){

    double gamma;
    double n = -1.0;
    double f = -100.0;

    // frustum corners
    double t;
    double b;
    double r;
    double l;

    switch (event->key()){
    case Qt::Key_W : {
        selectBoardRotation(0);
        break;
    }
    case Qt::Key_A : {
        selectBoardRotation(3);
        break;
    }
    case Qt::Key_S : {
        selectBoardRotation(2);
        break;
    }
    case Qt::Key_D : {
        selectBoardRotation(1);
        break;
    }
    case Qt::Key_Up : {
        camera_distance -= 0.5;
        if(camera_distance <= 0.5){
            camera_distance = 0.5;
        }
        break;
    }
    case Qt::Key_Down : {
        camera_distance += 0.5;
        break;
    }
    case Qt::Key_Space : {
        wireframe = !wireframe;
        break;
    }
    case Qt::Key_C : {
        switch (view) {
        case TOP_BOARD:
            view = FOLLOW_BALL;
            beta = 60.f;
            break;
        case FOLLOW_BALL:
            view = BALL;
            beta = 15.f;
            break;
        default: // = BALL
            view = TOP_BOARD;
            beta = 60.f;
            break;
        }
        /**********************MET's function************************/
        // recalculate camera opening angle
        // aspect ratio
        double gamma;
        if (height() > 0)
            gamma = width()/(double)height();
        else
            gamma = width();
        // front and back clipping plane at
        n = -1.0;
        f = -100.0;
        // frustum corners
        t = -tan(beta*3.14159/360.0) * n;
        b = -t;
        r = gamma * t;
        l = -r;
        // set projection matrix
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum( l,r , b,t , -n,-f );
        /**********************MET's function************************/
        break;
    }
    default:
        break;
    }
}

void CCanvas::mousePressEvent(QMouseEvent *event){
    last_mouse_pos = event->pos();
}

void CCanvas::mouseMoveEvent(QMouseEvent *event){
    int dx = event->x() - last_mouse_pos.x();
    int dy = event->y() - last_mouse_pos.y();
    tau -= dx / 100.0;
    if(tau < 0){
        tau = 2 * PI - tau;
    }else if(tau > 2 * PI){
        tau = tau - 2 * PI;
    }
    phi -= dy / 100.0;
    if(phi >= PI / 2.0 - 0.1){
        phi = PI / 2.0 - 0.1;
    }else if (phi <= -1 * (PI / 2.0 - 0.1)){
        phi = -1 * (PI / 2.0 - 0.1);
    }
    last_mouse_pos = event->pos();
}

void CCanvas::selectBoardRotation(int keyPressed){
    int newKey = 11;
    if((tau <= (PI / 4.0))){
        newKey = keyPressed;
    }else if((tau <=  (3 * PI / 4.0) ) && (tau > (PI / 4.0) )){
        newKey = keyPressed - 1;
        if(newKey == -1){
            newKey = 3;
        }
    }else if((tau <= (5 * PI / 4.0) ) && (tau > (3 * PI / 4.0) )){
        newKey = keyPressed - 2;
        if(newKey == -1){
            newKey = 3;
        }else if(newKey == -2){
            newKey = 2;
        }
    }else if((tau <= (7 * PI / 4.0) ) && (tau > (5 * PI / 4.0) )){
        newKey = keyPressed - 3;
        if(newKey == -1){
            newKey = 3;
        }else if(newKey == -2){
            newKey = 2;
        }else if(newKey == -3){
            newKey = 1;
        }
    }else if(tau > (7 * PI / 4.0)){
        newKey = keyPressed;
    }

    switch(newKey){
    case 0 : {
        b->rotateX(-0.2);
        break;
    }
    case 1 : {
        b->rotateZ(-0.2);
        break;
    }
    case 2 : {
        b->rotateX(0.2);
        break;
    }
    case 3 : {
        b->rotateZ(0.2);
        break;
    }
    default :
        break;
    }
}
