#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include <QVector2D>
class Wall;
class Sphere;

class CollisionDetector
{
public:
    /* Default constructor */
    CollisionDetector();

    static bool computeContinuosCollision(const Sphere& ball, const Wall& wall, const double& delta_t, std::pair<QVector2D, QVector2D>& hit_point);

    static bool computeCollision(const Sphere& ball, const Wall& wall, std::pair<QVector2D, QVector2D>& hit_point);

};

#endif // COLLISIONDETECTOR_H
