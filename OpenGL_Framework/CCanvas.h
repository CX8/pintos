/************************************************************************/
/* guards                                                               */
/************************************************************************/
#ifndef CCANVAS_H
#define CCANVAS_H

#include <iostream>
#include <QtOpenGL>
#include <QGLWidget>
#include <QTimer>

#include "Base.h"
#include "Parser.h"
#include "Boardgraphic.h"
#include "Sphereintegrator.h"

using namespace std;

enum VIEW {
    TOP_BOARD,
    FOLLOW_BALL,
    BALL
};

/************************************************************************/
/* Canvas to draw                                                       */
/************************************************************************/
class CCanvas : public QGLWidget
{
  Q_OBJECT

public:

  explicit CCanvas(QWidget *parent = 0) : QGLWidget(parent), tau(0.0), phi(PI /4.0), camera_distance(15.0), p(), beta(60.f), sphere_integrator(), wireframe(false), view(TOP_BOARD), end_game(false)
  {       
    QTimer *timer = new QTimer(this);
    this->setFocusPolicy(Qt::StrongFocus);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    timer->start(10);
  };

protected:

  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();
  void keyPressEvent(QKeyEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void selectBoardRotation(int keyPressed);
private:
  void lookAt(const GLdouble eyex,
                              const GLdouble eyey,
                              const GLdouble eyez,
                              const GLdouble centerx,
                              const GLdouble centery,
                              const GLdouble centerz,
                              const GLdouble upx,
                              const GLdouble upy,
                              const GLdouble upz);

  void glPerspective(
          const GLdouble fovy,
          const GLdouble aspect,
          const GLdouble zNear,
          const GLdouble zFar);
  // List of vertices and triangles
  vector< Point3d > v;
  struct Triangle { int v[3]; };
  vector< Triangle > t;

  QPoint last_mouse_pos;
  double tau;
  double phi;
  double camera_distance;
  /* Parser for pintos files */
  Parser p;
  /* Board of the game */
  Board* b;
  /* Sphere */
  Sphere ball;
  Sphere ball_copy;
  SphereIntegrator sphere_integrator;

  /* Shader program */
  QGLShaderProgram* program_directional;

  /* Wood texture id */
  GLuint wood_tex;
  GLuint wood_tex_wall;
  GLuint dark_wood;
  GLuint table_text;
  GLuint knob_text;

  double beta;

  bool end_game;

  /* Draw in wireframe mode */
  bool wireframe;

  VIEW view;

};
#endif 
