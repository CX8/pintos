#include "Knobgraphic.h"
#include "Boxgraphic.h"
#include "Board.h"
#include "Wall.h"
#include <iostream>
#include "Base.h"

KnobGraphic::KnobGraphic()
{
}

unsigned int KnobGraphic::generateList(const Board &board){
   double depth = 0.0;
           //-1 * std::max(board.getHeight(), board.getWidth()) / 6.0;
   double x_inner_center = 0.0;
           //board.getWidth() / 2.0 + BOX_DISTANCE + 2 * WALL_THICKNESS;
   double x_outer_center = x_inner_center + KNOB_LENGTH;
   double angle = 2 * PI / KNOB_COMPLEXITY;
   std::vector<Point3d> inner_circle;
   std::vector<Point3d> outer_circle;
   for(int i = 0; i <= KNOB_COMPLEXITY; i++){
       inner_circle.push_back(Point3d(x_inner_center, depth + cos(i * angle), sin(i * angle)));
       outer_circle.push_back(Point3d(x_outer_center, depth + cos(i * angle), sin(i * angle)));
   }
   GLuint index = glGenLists(1);
   glNewList(index, GL_COMPILE);
       glBegin(GL_TRIANGLE_FAN);
           glNormal3d(1.0, 0.0, 0.0);
           glTexCoord2d(0.5, 0.5);
           glVertex3d(x_outer_center, depth, 0.0);
           for(int j = 0; j < outer_circle.size(); j++){
               Point3d p = outer_circle.at(j);
               glTexCoord2d(0.5 -0.5 * sin(j * angle), 0.5 + 0.5 * cos(j * angle));
               glVertex3d(p.x(), p.y(), p.z());
           }
       glEnd();

       glBegin(GL_TRIANGLES);
           for(int k = 0; k < outer_circle.size() - 1; k++){
               Point3d inner_now = inner_circle.at(k);
               Point3d inner_next = inner_circle.at(k + 1);
               Point3d outer_now = outer_circle.at(k);
               Point3d outer_next = outer_circle.at(k + 1);
               Point3d normal = (outer_now - inner_next) ^ (inner_now - inner_next);
               normal.normalize();
               glNormal3d(normal.x(), normal.y(), normal.z());

               //texture like on the circumference of the cirlcle randomize do have a woody effect
               glTexCoord2d(0.5 -0.5 * sin(k * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos(k * angle));
               glVertex3d(inner_now.x(), inner_now.y(), inner_now.z() );
               glTexCoord2d(0.5 -0.5 * sin((k + 1) * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos((k + 1) * angle));
               glVertex3d(outer_next.x(), outer_next.y(), outer_next.z() );
               glTexCoord2d(0.5 -0.5 * sin(k * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos(k * angle));
               glVertex3d(outer_now.x(), outer_now.y(), outer_now.z() );

               glTexCoord2d(0.5 -0.5 * sin((k + 1) * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos((k + 1) * angle));
               glVertex3d(inner_next.x(), inner_next.y(), inner_next.z() );
               glTexCoord2d(0.5 -0.5 * sin((k + 1) * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos((k + 1) * angle));
               glVertex3d(outer_next.x(), outer_next.y(), outer_next.z() );
               glTexCoord2d(0.5 -0.5 * sin(k * angle + ((rand()- 0.5) / 10.0)), 0.5 + 0.5 * cos(k * angle));
               glVertex3d(inner_now.x(), inner_now.y(), inner_now.z() );
           }
       glEnd();
   glEndList();
   return index;
}

