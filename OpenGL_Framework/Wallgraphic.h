#ifndef WALLGRAPHIC_H
#define WALLGRAPHIC_H

#include <QtOpenGL>

class Wall;

class WallGraphic {
public:
    /* Default constructor */
    WallGraphic();

    /* Draw a given wall */
    static unsigned int generateList(const Wall& w);
};

#endif // WALLGRAPHIC_H
