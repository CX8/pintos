#ifndef TEXTURE_H
#define TEXTURE_H

#include <QtOpenGL>
#include <iostream>

/* Function to load a texture in OpenGL */
GLuint loadTexture(const char* file_name) {
    /* Generate id for the teture */
    GLuint id;
    glGenTextures(1, &id);

    QImage* image = new QImage();
    // Load image
    if(!image->load(QString(file_name))) {
        std::cout << "Error loading image" << std::endl;
        exit(EXIT_FAILURE);
    }

    QImage opengl_image = QGLWidget::convertToGLFormat(*image);

    /* Bind texture data */
    glBindTexture(GL_TEXTURE_2D, id);

    /* Set filters for texture */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    /* Copy data to texture */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, opengl_image.width(), opengl_image.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, opengl_image.bits());

    /* Check if we had a problem loading the image */
    GLuint err = glGetError();

    if (err != GL_NO_ERROR) {
        std::cout << "Error " << err << " with the loading of the texture " << std::string(file_name) << std::endl;
        exit(EXIT_FAILURE);
    }

    delete image;

    return id;
}

#endif // TEXTURE_H
