#include "Wall.h"
#include <iostream>

Wall::Wall()
    : start(), end() {}

Wall::Wall(const QVector2D &s, const QVector2D &e)
    : start(s), end(e), draw_list_id(WallGraphic::generateList(*this)) {}

void Wall::print() const {
    std::cout << "Start point (" << start.x() << ", " << start.y() << ")" << std::endl;
    std::cout << "End point (" << end.x() << ", " << end.y() << ")" << std::endl;
}

void Wall::draw(QGLShaderProgram* program, const unsigned int& wall_tex) const {
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, wall_tex);
    glCallList(draw_list_id);
    glBindTexture(GL_TEXTURE_2D, 0);
}
