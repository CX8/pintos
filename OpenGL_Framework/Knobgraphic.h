#ifndef KNOBGRAPHIC_H
#define KNOBGRAPHIC_H

#include <QtOpenGL>

class Board;

#define KNOB_COMPLEXITY 100.0
#define KNOB_LENGTH 1.0
#define KNOB_RADIUS 1.5
class KnobGraphic
{
public:
    KnobGraphic();

    static unsigned int generateList(const Board& board);
};

#endif // KNOBGRAPHIC_H
