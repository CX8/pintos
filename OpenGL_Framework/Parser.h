#ifndef PARSER_H
#define PARSER_H

#include <fstream>
#include <string>
#include <cstdio>
#include <vector>

#include "Board.h"

class Parser {
    // Read a file and put content into a vector of strings
    std::vector<std::string> readFile(const char* file_name);

public:
    /* Defualt constructor */
    Parser();

    /* Destructor */
    virtual ~Parser();

    /* Parse a pintos file containing a board */
    Board* parsePintosFile(const char* filename, Sphere& ball);
};

#endif // PARSER_H
