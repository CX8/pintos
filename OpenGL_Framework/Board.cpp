#include "Board.h"
#include "Boardpiece.h"
#include <iostream>

Board::Board()
    : width(10), height(10), angle_x(0.0), angle_z(0.0) {}

Board::Board(const unsigned int &w, const unsigned int &h)
    : width(w), height(h), angle_x(0.0), angle_z(0.0), box_list_id(BoxGraphic::generateList(*this)), knob_list_id(KnobGraphic::generateList(*this)),
      table_list_id(TableGraphic::generateList(*this)) {}

void Board::initBoard() {
    for (unsigned int r = 0; r < height; ++r) {
        for (unsigned int c = 0; c < width; c++) {
            bool is_hole = false;
            /* Check if current position is an hole */
            for (std::vector<std::pair<unsigned int, unsigned int> >::const_iterator it = holes.begin(); it != holes.end(); ++it) {
                if (r == it->first && c == it ->second) {
                    is_hole = true;
                }
            }
            if (is_hole) {
                board_pieces.push_back(new BoardPieceHole(r, c, height, width));
            } else {
                board_pieces.push_back(new BoardPieceFill(r, c, height, width));
            }
        }
    }
}

void Board::addWall(const Wall &w) {
    walls.push_back(w);
}

void Board::addHole(const unsigned int& r, const unsigned int& c) {
    holes.push_back(std::pair<unsigned int, unsigned int>(r, c));
}

void Board::print() const {
    std::cout << "### Board data ###" << std::endl;
    std::cout << "Width: " << width << " # Height: " << height << std::endl;

    std::cout << "## Wall data ##" << std::endl;
    unsigned int w_counter = 0;
    for (std::vector<Wall>::const_iterator it = walls.begin(); it != walls.end(); ++it) {
        std::cout << "Wall at position: " << w_counter << std::endl;
        it->print();
        w_counter++;
    }

    std::cout << "## Holes data ##" << std::endl;
    unsigned int h_counter = 0;
    for (std::vector<std::pair<unsigned int, unsigned int> >::const_iterator it = holes.begin(); it != holes.end(); ++it) {
        std::cout << "Hole at position: " << h_counter << std::endl;
        std::cout << "row: " << it->first << ", col: " << it->second << std::endl;
        h_counter++;
    }

}

void Board::rotateX(const double& angle_x){
    this->angle_x += angle_x;
   if( this->angle_x >= ANGLE_LIMIT){
       this->angle_x = ANGLE_LIMIT;
   }else if(this->angle_x <= -ANGLE_LIMIT){
       this->angle_x = -ANGLE_LIMIT;
   }
}

void Board::rotateZ(const double& angle_z){
    this->angle_z += angle_z;
    if( this->angle_z >= ANGLE_LIMIT){
        this->angle_z = ANGLE_LIMIT;
    }else if(this->angle_z <= -ANGLE_LIMIT){
        this->angle_z = -ANGLE_LIMIT;
    }
}

bool Board::draw(QGLShaderProgram* program, Sphere& ball, const unsigned int& board_tex, const unsigned int& wall_tex, const bool sphere_falling) const {
    glPushMatrix();
    glRotated(angle_x, 1,0,0);
    glRotated(angle_z, 0,0,1);
    /* Draw board pieces */
    for (std::vector<BoardPiece*>::const_iterator it = board_pieces.begin(); it != board_pieces.end(); ++ it) {
        (*it)->draw(program, board_tex);
    }
    /* Draw walls */
    for(std::vector<Wall>::const_iterator it = walls.begin(); it != walls.end(); ++it) {
        it->draw(program, wall_tex);
    }
    /* Draw ball */
    bool end_animation = ball.draw(program, sphere_falling);

    glPopMatrix();

    return end_animation;
}

void Board::drawBox(QGLShaderProgram *program, const unsigned int& board_tex) const {
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, board_tex);
    glCallList(box_list_id);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Board::drawTable(QGLShaderProgram* program, const unsigned int& table_tex) const{
    glPushMatrix();
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, table_tex);
    glCallList(table_list_id);
    glBindTexture(GL_TEXTURE_2D, 0);
    glPopMatrix();
}

void Board::drawKnobs(QGLShaderProgram *program, const unsigned int& knob_tex) const{
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, knob_tex);

    double x_coord = this->width / 2.0 + BOX_DISTANCE + 2 * WALL_THICKNESS;
    double y_coord = -1 * std::max(this->width, this->height)/ 6.0;
    double z_coord = this->height / 2.0 + BOX_DISTANCE + 2 * WALL_THICKNESS;
    glPushMatrix();
    glTranslated(x_coord, y_coord, 0);
    glRotated(10.0 * angle_x, 1,0,0);
    glCallList(knob_list_id);
    glPopMatrix();
    glPushMatrix();
    glRotated(-90.0, 0,1,0);
    glTranslated(z_coord, y_coord, 0);
    glRotated(10.0 * angle_z, 1,0,0);
    glCallList(knob_list_id);
    glBindTexture(GL_TEXTURE_2D, 0);
    glPopMatrix();

}

const float &Board::getWidth() const{
    return this->width;
}

const float &Board::getHeight() const{
    return this->height;
}

const std::vector<Wall>& Board::getWalls() const {
    return walls;
}

 const std::vector<std::pair<unsigned int, unsigned int> >& Board::getHoles() const {
     return holes;
 }

const double& Board::getXAngle() const {
    return angle_x;
}

const double& Board::getZAngle() const {
    return angle_z;
}

void Board::reset_board() {
    angle_x = 0;
    angle_z = 0;
}

