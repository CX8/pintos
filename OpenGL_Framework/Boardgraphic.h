#ifndef BOARDGRAPHIC_H
#define BOARDGRAPHIC_H

#include "Wallgraphic.h"
#include <QtOpenGL>

class Board;

class BoardGraphic {
public:
    /* Default constructor */
    BoardGraphic();

    /* Generate the graphic of the board */
    static unsigned int generateList(const Board& board);
};

#endif // BOARDGRAPHIC_H
