#ifndef BOXGRAPHIC_H
#define BOXGRAPHIC_H

#include <QtOpenGL>

#define BOX_DISTANCE 0.35

class Board;

class BoxGraphic
{
public:
    BoxGraphic();

    static unsigned int generateList(const Board& board);
};

#endif // BOXGRAPHIC_H
