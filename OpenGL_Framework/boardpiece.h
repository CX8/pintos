#ifndef BOARDPIECE_H
#define BOARDPIECE_H

#include <QGLShaderProgram>

class BoardPiece
{
protected:
    unsigned int display_list_id;
    unsigned int row;
    unsigned int col;
    unsigned int num_row;
    unsigned int num_col;
public:
    /* Default constructor */
    BoardPiece();

    /* Paramether constructor */
    BoardPiece(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col, const unsigned int id);

    /* Destructor */
    virtual ~BoardPiece();

    /* Pure virtual draw method */
    virtual void draw(QGLShaderProgram* program, const unsigned int& board_tex) const = 0;
};

class BoardPieceFill : public BoardPiece {
public:
    /* Default constructor */
    BoardPieceFill();

    /* Paramether constructor */
    BoardPieceFill(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col);

    /* Destructor */
    virtual ~BoardPieceFill();

    /* Pure virtual draw method */
    virtual void draw(QGLShaderProgram* program, const unsigned int& board_tex) const;

};

class BoardPieceHole : public BoardPiece {
public:
    /* Default constructor */
    BoardPieceHole();

    /* Paramether constructor */
    BoardPieceHole(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col);

    /* Destructor */
    virtual ~BoardPieceHole();

    /* Pure virtual draw method */
    virtual void draw(QGLShaderProgram* program, const unsigned int& board_tex) const;
};

#endif // BOARDPIECE_H
