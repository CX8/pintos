#ifndef SPHEREINTEGRATOR_H
#define SPHEREINTEGRATOR_H

#include "Sphere.h"
#include "Board.h"

class SphereIntegrator
{
    QVector3D gravity;
public:
    SphereIntegrator();

    void computeStep(Sphere& ball, const Board& board, const double& delta_t);

    bool checkEndGame(Sphere& ball, const Board& board);
};

#endif // SPHEREINTEGRATOR_H
