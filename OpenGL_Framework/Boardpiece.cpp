#include "Boardpiece.h"
#include "Piecegraphic.h"
#include <iostream>

/* Base abstract board piece class */
BoardPiece::BoardPiece() {}

BoardPiece::BoardPiece(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col, const unsigned int id)
    :   display_list_id(id), row(r), col(c), num_row(num_row), num_col(num_col) {}

BoardPiece::~BoardPiece() {}

/* Filled board piece class */
BoardPieceFill::BoardPieceFill()
    : BoardPiece() {}

BoardPieceFill::BoardPieceFill(const unsigned int &r, const unsigned int &c, const unsigned int& num_row, const unsigned int& num_col)
    : BoardPiece(r, c, num_row, num_col, PieceGraphic::generateListFill(r, c, num_row, num_col)) {}

BoardPieceFill::~BoardPieceFill() {}

void BoardPieceFill::draw(QGLShaderProgram* program, const unsigned int& board_tex) const {
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, board_tex);
    glPushMatrix();
    glTranslated(-static_cast<float>(num_col) / 2 + 0.5 + col, 0, static_cast<float>(num_row) / 2 - 0.5 - row);
    glCallList(display_list_id);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

/* Board piece with hole */
BoardPieceHole::BoardPieceHole()
    : BoardPiece() {}

BoardPieceHole::BoardPieceHole(const unsigned int &r, const unsigned int &c, const unsigned int& num_row, const unsigned int& num_col)
    : BoardPiece(r, c, num_row, num_col, PieceGraphic::generateListHole(r, c, num_row, num_col)) {}

BoardPieceHole::~BoardPieceHole() {}

void BoardPieceHole::draw(QGLShaderProgram* program, const unsigned int& board_tex) const {
    program->setUniformValue("use_tex", true);
    glBindTexture(GL_TEXTURE_2D, board_tex);
    glPushMatrix();
    glTranslated(-static_cast<float>(num_col) / 2 + 0.5 + col, 0, static_cast<float>(num_row) / 2 - 0.5 - row);
    glCallList(display_list_id);
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}
