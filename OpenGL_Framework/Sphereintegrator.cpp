#include "Sphereintegrator.h"
#include "Collisiondetector.h"
#include <QMatrix4x4>
#include <QQuaternion>
#include <iostream>

SphereIntegrator::SphereIntegrator()
    : gravity(0, -9.81, 0) {}

void SphereIntegrator::computeStep(Sphere &ball, const Board &board, const double& delta_t) {
    /* Compute normal of the plane according to rotations */
    QVector3D y_axis_neg = QVector3D(0, -1, 0);
    /* Create quaternions */
    QQuaternion x_quaternion = QQuaternion((-board.getXAngle() / 180.0) * M_PI, QVector3D(1, 0, 0));
    QQuaternion z_quaternion = QQuaternion((board.getZAngle() / 180.0) * M_PI, QVector3D(0, 0, 1));
    /* Transform the axis */
    QMatrix4x4 rotation_matrix;
    rotation_matrix.rotate(x_quaternion);
    rotation_matrix.rotate(z_quaternion);
    /* Compute transformed normal */
    QVector3D normal = rotation_matrix * y_axis_neg;
    /* Project the gravity on the normal */
    QVector3D projection = (QVector3D::dotProduct(gravity, normal) / normal.lengthSquared()) * normal;
    /* Compute force on the ball */
    QVector3D force = gravity - projection;
    force.setY(0);

    /* Compute step on the ball */
    ball.acceleration = force / ball.mass - 0.2 * ball.speed.lengthSquared() * ball.speed;
    ball.speed += ball.acceleration * delta_t;

    /* Call solver for collisions */
    /* If move step is big, compute continuos collisions */
    if ((ball.speed * delta_t).length() > ball.radius * 2) {
        for (std::vector<Wall>::const_iterator wall_it = board.getWalls().begin(); wall_it != board.getWalls().end(); ++wall_it) {
            std::pair<QVector2D, QVector2D> collision_point;
            bool collison = CollisionDetector::computeContinuosCollision(ball, *wall_it, delta_t, collision_point);
            if (collison) {
                std::cout << "Detected continuos collision " << collision_point.first.x() << " " << collision_point.first.y() << std::endl;
            }
        }
    }
    /* Still need to implement what to do in case of continous collision detected */

    /* Compute simple collision based on distance */
    bool collision = false;
    do {
        for (std::vector<Wall>::const_iterator wall_it = board.getWalls().begin(); wall_it != board.getWalls().end(); ++wall_it) {
            std::pair<QVector2D, QVector2D> collision_point;
            collision = CollisionDetector::computeCollision(ball, *wall_it, collision_point);
            if (collision) {
//                std::cout << "Detected simple collision " << collision_point.first.x() << " " << collision_point.first.y() << std::endl;
                /* Reflect speed */
//                std::cout << "Ball speed: " << ball.speed.x() << " " << ball.speed.z() << std::endl;

                ball.speed = 0.8 * (ball.speed - (2 * QVector3D::dotProduct(ball.speed, QVector3D(collision_point.second.x(), 0, collision_point.second.y()))) * QVector3D(collision_point.second.x(), 0, collision_point.second.y()));

//                std::cout << "New ball speed: " << ball.speed.x() << " " << ball.speed.z() << std::endl;

                /* Update position */
                QVector2D ball_center_to_collision = collision_point.first - QVector2D(ball.position.x(), ball.position.z());
                double distance = ball_center_to_collision.length();
//                std::cout << "Distance: " << distance << std::endl;
                double correction = (ball.radius - distance) + 10e-5;
//                std::cout << "Correction " << correction <<std::endl;
                QVector2D plane_correction = correction * collision_point.second;
                QVector3D plane_corection_3D = QVector3D(plane_correction.x(), 0, plane_correction.y());
//                std::cout << "Ball position: " << ball.position.x() << " " << ball.position.z() << std::endl;
                ball.position += plane_corection_3D;
//                std::cout << "Corrected ball position: " << ball.position.x() << " " << ball.position.z() << std::endl;

            }
        }
    } while(collision);

//    std::cout << "Speed: " << ball.speed.x() << " " << ball.speed.z() << std::endl;

    /* Compute new ball position */
    ball.position += ball.speed * delta_t;

}

bool SphereIntegrator::checkEndGame(Sphere& ball, const Board& board) {
    QVector2D ball_position_plane = QVector2D(ball.position.x(), ball.position.z());
//    std::cout << board.getHoles().size() << std::endl;
    for (std::vector<std::pair<unsigned int, unsigned int> >::const_iterator it = board.getHoles().begin(); it != board.getHoles().end(); ++it) {
        QVector2D hole_center = QVector2D(-4.5 + it->second, 4.5 - it->first);
//        std::cout << "Hole center: " << hole_center.x() << " " << hole_center.y() << std::endl;
        if ((ball_position_plane - hole_center).length() < 0.35) {
            return true;
        }
    }

    return false;
}
