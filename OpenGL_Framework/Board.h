#ifndef BOARD_H
#define BOARD_H

#include <vector>
#include <utility>
#include "Wall.h"
#include "Boxgraphic.h"
#include "Tablegraphic.h"
#include "Boardpiece.h"
#include "Sphere.h"
#include "Knobgraphic.h"

#define ANGLE_LIMIT 6.0

class Board {
    /* Add BoardGraphic as friend so we can access directly the attributes */
    friend class BoardGraphic;
    friend class BoxGraphic;
    friend class TableGraphic;

    /* width and height of the board */
    float width;
    float height;

    /* Rotation of the board around x and z axis */
    double angle_x;
    double angle_z;

    /* Vector with all the walls of the board */
    std::vector<Wall> walls;

    /* List of pieces which have a hole */
    std::vector<std::pair<unsigned int, unsigned int> > holes;

    /* List with all the pieces of the board */
    std::vector<BoardPiece*> board_pieces;

    /*Id of the box */
    unsigned int box_list_id;

    /*Id of the knob */
    unsigned int knob_list_id;

    /*Id of the Table */
    unsigned int table_list_id;

public:
    /* Defualt constructor */
    Board();

    /* Paramether constructor */
    Board(const unsigned int& w, const unsigned int& h);

    /* Add a wall to the board */
    void addWall(const Wall& w);

    /* Add a hole to the box */
    void addHole(const unsigned int& r, const unsigned int& c);

    /* Rotate the board alng the axis */
    void rotateX(const double& angle_x);
    void rotateZ(const double& angle_z);

    /* Print out board data (debug function) */
    void print() const;

    /* Initialize board pieces */
    void initBoard();

    const float &getWidth() const;

    const float &getHeight() const;

    /* Getter for the list of walls for the physic part */
    const std::vector<Wall>& getWalls() const;

    /* Getter for the list of holes */
    const std::vector<std::pair<unsigned int, unsigned int> >& getHoles() const;

    /* Getters for angles of the board */
    const double& getXAngle() const;

    const double& getZAngle() const;

    void reset_board();

    /* Draw the board */
    bool draw(QGLShaderProgram* program, Sphere& ball, const unsigned int& board_tex, const unsigned int& wall_tex, const bool sphere_falling) const;

    /* Draw the box */
    void drawBox(QGLShaderProgram* program, const unsigned int& board_tex) const;

    /* Draw the table */
    void drawTable(QGLShaderProgram* program, const unsigned int& table_tex) const;

    void drawKnobs(QGLShaderProgram* program, const unsigned int& knob_text) const;
};

#endif // BOARD_H

