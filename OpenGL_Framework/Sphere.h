#ifndef SPHERE_H
#define SPHERE_H

#include <QVector3D>
#include <QGLShaderProgram>

class SphereIntegrator;
class CollisionDetector;
class CCanvas;

class Sphere {
    friend class SphereIntegrator;
    friend class CollisionDetector;
    friend class CCanvas;
    /* Display list id */
    unsigned int sphere_list_id;
    /* Sphere attributes */
    double mass;
    double radius;
    QVector3D position;
    QVector3D speed;
    QVector3D acceleration;

public:
    /* Default constructor */
    Sphere();

    /* Paramether constructor */
    Sphere(const double& m, const double& r, const QVector3D& p, const QVector3D& s, const QVector3D& a);

    /* Destructor */
    virtual ~Sphere();

    /* Draw the sphere */
    bool draw(QGLShaderProgram* program, const bool is_falling);
};

#endif // SPHERE_H
