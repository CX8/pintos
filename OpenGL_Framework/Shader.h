#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <qglshaderprogram.h>

QGLShaderProgram* createProgram(QGLWidget* parent, const QString& vertex_shader_name, const QString& fragment_shader_name) {
    QGLShaderProgram* program = new QGLShaderProgram(parent);

    /* Create vertex shader */
    QGLShader* vs = new QGLShader(QGLShader::Vertex, parent);
    vs->compileSourceFile(vertex_shader_name);

    /* Create fragment shader */
    QGLShader *fs = new QGLShader(QGLShader::Fragment, parent);
    fs->compileSourceFile(fragment_shader_name);

    /* Add shader to the program */
    program->addShader(vs);
    program->addShader(fs);

    if (!program->link()) {
        std::cout << "Failed link shaders" << std::endl;
        QString err = program->log();
        std::cout << err.toUtf8().constData() << std::endl;
        exit(EXIT_FAILURE);
    }

    return program;
}

#endif // SHADER_H
