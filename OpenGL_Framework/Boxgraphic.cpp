#include "Boxgraphic.h"
#include "Board.h"
#include "Wall.h"
#include <iostream>

BoxGraphic::BoxGraphic(){}

unsigned int BoxGraphic::generateList(const Board& board) {
    double box_depth = std::max(board.width, board.height) / 3.0;
    double internal_walls_width = board.width/2.0 + BOX_DISTANCE;
    double internal_walls_height = board.height / 2.0 + BOX_DISTANCE;
    double box_top = 2 * WALL_HEIGHT;
    double box_internal_bottom = -1 * box_depth;
    double external_walls_width = board.width/2.0 + BOX_DISTANCE + 2 * WALL_THICKNESS;
    double external_walls_height = board.height / 2.0 + BOX_DISTANCE + 2 * WALL_THICKNESS;
    double box_external_bottom = -1 * (box_depth + 2 * WALL_THICKNESS);
    GLuint index = glGenLists(1);
    glNewList(index, GL_COMPILE);
        glBegin(GL_TRIANGLES);


        //internal walls
        /*************************************/
        //face in  +z
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top , internal_walls_height );
        glTexCoord2d(1,1);
        glVertex3d(internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(1,0);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, internal_walls_height);

        glTexCoord2d(0,1);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1,1);
        glVertex3d(internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(0,0);
        glVertex3d(-1 * internal_walls_width, box_top , internal_walls_height );;

        //face in  +x
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(internal_walls_width, box_internal_bottom, internal_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );

        //face in -z
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(internal_walls_width, -1 * box_depth, -1 * internal_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );

        //face in -x
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, -1 * internal_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );

        //bottom
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(internal_walls_width, box_internal_bottom, internal_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * internal_walls_width, box_internal_bottom, internal_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_internal_bottom, -1 * internal_walls_height );


        //external walls
        /************************************/
        //face in  +z
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * external_walls_width, box_top , external_walls_height );
        glTexCoord2d(1,1);
        glVertex3d(external_walls_width, box_external_bottom, external_walls_height );
        glTexCoord2d(1,0);
        glVertex3d(-1 * external_walls_width, box_external_bottom, external_walls_height);

        glTexCoord2d(0,1);
        glVertex3d(external_walls_width, box_top, external_walls_height );
        glTexCoord2d(1,1);
        glVertex3d(external_walls_width, box_external_bottom, external_walls_height );
        glTexCoord2d(0,0);
        glVertex3d(-1 * external_walls_width, box_top , external_walls_height );;

        //face in  +x
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_top, external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(external_walls_width, box_external_bottom, external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_top, external_walls_height );

        //face in -z
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(external_walls_width, box_external_bottom, -1 * external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );

        //face in -x
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(-1 * external_walls_width, box_external_bottom, -1 * external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * external_walls_width, box_top, external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, external_walls_height);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );

        //bottom
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(external_walls_width, box_external_bottom, external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, -1 * external_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_external_bottom, external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(external_walls_width, box_external_bottom, -1 * external_walls_height );


        //upper corners
        /**************************************/
        //corner in +z
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_top, external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(external_walls_width, box_top, external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_top, external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );

        //corner in +x
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_top, external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_top, external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );

        //corner in -z
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );

        //corner in -x
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(1, 0);
        glVertex3d(-1 * external_walls_width, box_top, external_walls_height );

        glTexCoord2d(0, 1);
        glVertex3d(-1 * internal_walls_width, box_top, -1 * internal_walls_height );
        glTexCoord2d(1, 1);
        glVertex3d(-1 * external_walls_width, box_top, -1 * external_walls_height );
        glTexCoord2d(0, 0);
        glVertex3d(-1 * internal_walls_width, box_top, internal_walls_height );

        glEnd();

    glEndList();
    return index;
}
