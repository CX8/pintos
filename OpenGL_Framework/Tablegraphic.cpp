#include "Tablegraphic.h"
#include "Board.h"
#include "Wall.h"
#include "Boxgraphic.h"
#include <QtOpenGl>



TableGraphic::TableGraphic(){}

unsigned int TableGraphic::generateList(const Board& board){
    double box_depth = std::max(board.width, board.height) / 3.0;
    double box_external_bottom = -1 * (box_depth + 2 * WALL_THICKNESS);
    GLuint index = glGenLists(1);
    glNewList(index, GL_COMPILE);
        glBegin(GL_TRIANGLES);

        //top of the board
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0,0);
        glVertex3d(-board.width * 2, box_external_bottom, -board.height * 2);
        glTexCoord2d(1,0);
        glVertex3d(-board.width * 2, box_external_bottom, board.height * 2);
        glTexCoord2d(1,1);
        glVertex3d(board.width * 2, box_external_bottom, board.height * 2);

        glTexCoord2d(0,1);
        glVertex3d(board.width * 2, box_external_bottom, -board.height * 2);
        glTexCoord2d(0,0);
        glVertex3d(-board.width * 2, box_external_bottom, -board.height * 2);
        glTexCoord2d(1,1);
        glVertex3d(board.width * 2, box_external_bottom, board.height * 2);


        //top corner
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(1, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom, -board.height * 2.0 );
        glTexCoord2d(1, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0);
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0);

        glTexCoord2d(0, 0);
        glVertex3d(board.width * 2.0, box_external_bottom, -board.height * 2.0);
        glTexCoord2d(1, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom, -board.height * 2.0);
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0);


        //right corner
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0);
        glTexCoord2d(1, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2.0, box_external_bottom, -board.height * 2.0);
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2, box_external_bottom, board.height * 2);

        glTexCoord2d(0, 0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width * 2.0, box_external_bottom-TABLE_THICKNESS, -board.height * 2.0 );
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2.0, box_external_bottom, board.height * 2.0);


        //bottom corner
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );
        glTexCoord2d(1, TABLE_THICKNESS / 10.0);
        glVertex3d(board.width * 2.0, box_external_bottom, board.height * 2.0 );
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom, board.height * 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom, board.height * 2.0 );


        //left corner
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(1, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );
        glTexCoord2d(1, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom, board.height * 2.0 );
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom, -board.height * 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0 );
        glTexCoord2d(1, 0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );
        glTexCoord2d(0, TABLE_THICKNESS / 10.0);
        glVertex3d(-board.width * 2.0, box_external_bottom, -board.height * 2.0 );


        //bottom of the board
        glNormal3d(0.0, -1.0, 0.0);
        glTexCoord2d(0,0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0 );
        glTexCoord2d(0,1);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0 );
        glTexCoord2d(1,0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );

        glTexCoord2d(0,1);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, -board.height * 2.0 );
        glTexCoord2d(1,1);
        glVertex3d(board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );
        glTexCoord2d(1,0);
        glVertex3d(-board.width * 2.0, box_external_bottom - TABLE_THICKNESS, board.height * 2.0 );

        glEnd();

    glEndList();
    return index;
}
