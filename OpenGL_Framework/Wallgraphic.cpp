#include "Wallgraphic.h"
#include "Wall.h"
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QMatrix4x4>
#include <iostream>

WallGraphic::WallGraphic() {}

unsigned int WallGraphic::generateList(const Wall &w) {

    QMatrix4x4 rotation_matrix = QMatrix4x4::QMatrix4x4 ();
    rotation_matrix.rotate(90, QVector3D (0.0, 1.0, 0.0));

    QVector4D move_vector (w.end.x() - w.start.x(), 0.0, w.end.y() - w.start.y(), 0.0);
    QVector4D original_direction (w.end.x() - w.start.x(), 0.0, w.end.y() - w.start.y(), 0.0);

    //make the vector the right size
    move_vector.normalize();
    move_vector *= WALL_THICKNESS/2.0;

    // going "back"
    move_vector = rotation_matrix *move_vector;

    QVector3D back_wall_start (w.start.x() - move_vector.x(), 0.0, w.start.y() - move_vector.z());
    QVector3D back_wall_end (w.end.x() - move_vector.x(), 0.0, w.end.y() - move_vector.z());

    // going "front"
    QVector3D front_wall_start (w.start.x() + move_vector.x(), 0.0, w.start.y() + move_vector.z());
    QVector3D front_wall_end (w.end.x() + move_vector.x(), 0.0, w.end.y() + move_vector.z());

    int height = rand() % 10;

    double wall_leght = (w.end - w.start).length();

    GLuint index = glGenLists(1);
    glNewList(index, GL_COMPILE);
    //triangles
        glBegin(GL_TRIANGLES);
        //front side
        move_vector.normalize();
        glNormal3d(move_vector.x(), move_vector.y(), move_vector.z());

        glTexCoord2d(0, 0.1 * (height + 1));
        glVertex3d(front_wall_start.x(), front_wall_start.y() + WALL_HEIGHT, front_wall_start.z());
        glTexCoord2d(0, 0.1 * height);
        glVertex3d(front_wall_start.x(), front_wall_start.y(), front_wall_start.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * height);
        glVertex3d(front_wall_end.x(), front_wall_end.y(), front_wall_end.z());


        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height + 1));
        glVertex3d(front_wall_end.x(), front_wall_end.y() + WALL_HEIGHT, front_wall_end.z());
        glTexCoord2d(0, 0.1 * (height + 1));
        glVertex3d(front_wall_start.x(), front_wall_start.y() + WALL_HEIGHT, front_wall_start.z());
        glTexCoord2d(0.1 * wall_leght, 0.1 * height);
        glVertex3d(front_wall_end.x(), front_wall_end.y(), front_wall_end.z());


        //back side
        glNormal3d(-move_vector.x(), -move_vector.y(), -move_vector.z());

        height = rand() % 10;

        glTexCoord2d( 0, 0.1 * height);
        glVertex3d(back_wall_end.x(), back_wall_end.y(), back_wall_end.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * height );
        glVertex3d(back_wall_start.x(), back_wall_start.y(), back_wall_start.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height + 1));
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());

        glTexCoord2d( 0, 0.1 * (height + 1));
        glVertex3d(back_wall_end.x(), back_wall_end.y() + WALL_HEIGHT, back_wall_end.z());
        glTexCoord2d( 0, 0.1 * height);
        glVertex3d(back_wall_end.x(), back_wall_end.y(), back_wall_end.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height + 1));
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());


        //left side
        move_vector=rotation_matrix.map(move_vector);
        glNormal3d(move_vector.x(), move_vector.y(), move_vector.z());

        height = rand() % 10;

        glTexCoord2d( 0, 0.1 * (height + 1));
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());
        glTexCoord2d( 0, 0.1 * height);
        glVertex3d(back_wall_start.x(), back_wall_start.y(), back_wall_start.z());
        glTexCoord2d( 0.1 * 0.2, 0.1 * height);
        glVertex3d(front_wall_start.x(), front_wall_start.y(), front_wall_start.z());

        glTexCoord2d( 0.1 * 0.2, 0.1 * (height + 1));
        glVertex3d(front_wall_start.x(), front_wall_start.y() + WALL_HEIGHT, front_wall_start.z());
        glTexCoord2d( 0, 0.1 * (height + 1));
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());
        glTexCoord2d( 0.1 * 0.2, 0.1 * height);
        glVertex3d(front_wall_start.x(), front_wall_start.y(), front_wall_start.z());


        //right side
        glNormal3d(-move_vector.x(), -move_vector.y(), -move_vector.z());

        height = rand() % 10;

        glTexCoord2d( 0, 0.1 * (height + 1));
        glVertex3d(front_wall_end.x(), front_wall_end.y() + WALL_HEIGHT, front_wall_end.z());
        glTexCoord2d( 0, 0.1 * height);
        glVertex3d(front_wall_end.x(), front_wall_end.y(), front_wall_end.z());
        glTexCoord2d( 0.1 * 0.2, 0.1 * height);
        glVertex3d(back_wall_end.x(), back_wall_end.y(), back_wall_end.z());

        glTexCoord2d( 0.1 * 0.2, 0.1 * (height + 1));
        glVertex3d(back_wall_end.x(), back_wall_end.y() + WALL_HEIGHT, back_wall_end.z());
        glTexCoord2d( 0, 0.1 * (height + 1));
        glVertex3d(front_wall_end.x(), front_wall_end.y() + WALL_HEIGHT, front_wall_end.z());
        glTexCoord2d( 0.1 * 0.2, 0.1 * height);
        glVertex3d(back_wall_end.x(), back_wall_end.y(), back_wall_end.z());


        //top side

        move_vector = rotation_matrix.map(move_vector);
        move_vector= QVector3D::crossProduct(move_vector.toVector3D(),original_direction.toVector3D());
        move_vector.normalize();
        glNormal3d(move_vector.x(), move_vector.y(), move_vector.z());

        height = rand() % 10;

        glTexCoord2d( 0, 0.1 * (height) + 0.1 * 0.2);
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());
        glTexCoord2d( 0, 0.1 * height);
        glVertex3d(front_wall_start.x(), front_wall_start.y() + WALL_HEIGHT, front_wall_start.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height));
        glVertex3d(front_wall_end.x(), front_wall_end.y() + WALL_HEIGHT, front_wall_end.z());

        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height) + 0.1 * 0.2);
        glVertex3d(back_wall_end.x(), back_wall_end.y() + WALL_HEIGHT, back_wall_end.z());
        glTexCoord2d( 0, 0.1 * (height) + 0.1 * 0.2);
        glVertex3d(back_wall_start.x(), back_wall_start.y() + WALL_HEIGHT, back_wall_start.z());
        glTexCoord2d( 0.1 * wall_leght, 0.1 * (height));
        glVertex3d(front_wall_end.x(), front_wall_end.y() + WALL_HEIGHT, front_wall_end.z());


        glEnd();

    glEndList();
    return index;
}
