#ifndef PIECEGRAPHIC_H
#define PIECEGRAPHIC_H

#include <QVector2D>

class Board;

class PieceGraphic
{
public:
    PieceGraphic();

    /* Generate the graphic of one piece of the board filled */
    static unsigned int generateListFill(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col);

    /* Generate the graphic of one piece of the board with the hole */
    static unsigned int generateListHole(const unsigned int& r, const unsigned int& c, const unsigned int& num_row, const unsigned int& num_col);
};

#endif // PIECEGRAPHIC_H
