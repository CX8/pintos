#ifndef WALL_H
#define WALL_H

#include <QVector2D>
#include "Wallgraphic.h"
#define WALL_THICKNESS 0.2
#define WALL_HEIGHT 1.0

class CollisionDetector;

class Wall {
    friend class CollisionDetector;
    /* Start of the wall */
    QVector2D start;
    /* End of the wall */
    QVector2D end;

    /* Add WallGraphic as friend so we can access directly the attributes */
    friend class WallGraphic;

    /* Id of the display list for OpenGL */
    unsigned int draw_list_id;
public:
    /* Default constructor */
    Wall();

    /* Paramether constructor */
    Wall(const QVector2D& s, const QVector2D& e);

    /* Print out wall data (debug function) */
    void print() const;

    /* Draw the wall */
    void draw(QGLShaderProgram* program, const unsigned int& wall_tex) const;
};

#endif // WALL_H
