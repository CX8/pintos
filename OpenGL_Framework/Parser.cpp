#include "Parser.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

Parser::Parser() {}

Parser::~Parser() {}

Board* Parser::parsePintosFile(const char *filename, Sphere& ball) {

    std::vector<std::string> loaded_file = readFile(filename);

    if (loaded_file.empty()) {
        std::cout << "Board file loaded was empty" << std::endl;
        exit(EXIT_FAILURE);
    }

    // check if file is a valid pintos file
    if (loaded_file[0].compare(0,5,"BOARD") != 0) {
        std::cout << "This is not a valid Pintos file" << std::endl;
        exit(EXIT_FAILURE);
    }

    // read width and height
    unsigned int w, h;
    if(sscanf(loaded_file[1].c_str(), "w %d h %d", &w, &h) != 2) {
        std::cout << "Failed to load width and height" << std::endl;
        exit(EXIT_FAILURE);
    }

    Board* board = new Board(w, h);

    /* Read ball position */
    float x, y;
    if(sscanf(loaded_file[2].c_str(), "b %f %f", &x, &y) != 2) {
        std::cout << "Failed to load ball position" << std::endl;
        exit(EXIT_FAILURE);
    }

    ball = Sphere(10.0, 0.3, QVector3D(x, 0.3, y), QVector3D(), QVector3D());

    // read number of walls
    unsigned int n_w;
    if(sscanf(loaded_file[3].c_str(), "walls %d", &n_w) != 1) {
        std::cout << "Failed to load number of walls" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (n_w != 0) {
        // read all the walls
        for (unsigned int i = 0; i < n_w; ++i) {
            float x1, z1, x2, z2;
            if (sscanf(loaded_file[4 + i].c_str(), "m %f %f %f %f", &x1, &z1, &x2, &z2) != 4) {
                std::cout << "Failed to load wall data" << std::endl;
                exit(EXIT_FAILURE);
            }
            board->addWall(Wall(QVector2D(x1, z1), QVector2D(x2, z2)));
        }
    }

    /* Read number of holes */
    unsigned int n_h;
    if(sscanf(loaded_file[4 + n_w].c_str(), "holes %d", &n_h) != 1) {
        std::cout << "Failed to load number of holes" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (n_h != 0) {
        // read all the holes
        for (unsigned int i = 0; i < n_h; ++i) {
            int x, z;
            if (sscanf(loaded_file[4 + n_w + 1 + i].c_str(), "h %d %d", &x, &z) != 2) {
                std::cout << "Failed to load hole data" << std::endl;
                exit(EXIT_FAILURE);
            }
            board->addHole(x, z);
        }
    }

    /* Clear vector with loaded file */
    loaded_file.clear();

    /* Before return, init the board and print data of the loaded board */
    board->initBoard();
    board->print();
    return board;

}

std::vector<std::string> Parser::readFile(const char *file_name) {

    std::vector<std::string> file_lines;
    char buffer[256];

    fprintf(stdout, "Reading file %s\n", file_name);

    std::ifstream in_file(file_name);
    if (!in_file.is_open()) {
        fprintf(stdout, "Could not open file %s\n", file_name);
        exit(EXIT_FAILURE);
    }

    while (!in_file.eof()) {
        in_file.getline(buffer, 256);
        file_lines.push_back(std::string(buffer));
    }

    in_file.close();

    return file_lines;
}

