#ifndef TABLE_H
#define TABLE_H
#include <QtOpenGl>

#define TABLE_THICKNESS 2.0

class Board;

class TableGraphic {
public:
    /* Default constructor */
    TableGraphic();

    /* Generate the graphic of the board */
    static unsigned int generateList(const Board& board);

};

#endif // TABLE_H



//class BoardGraphic {
//public:
//    /* Default constructor */
//    BoardGraphic();

//    /* Generate the graphic of the board */
//    static unsigned int generateList(const Board& board);
//};
