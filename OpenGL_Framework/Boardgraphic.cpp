#include "Boardgraphic.h"
#include "Board.h"
#include <iostream>
#include <QtOpenGL>

BoardGraphic::BoardGraphic() {}

unsigned int BoardGraphic::generateList(const Board& board) {
    GLuint index = glGenLists(1);
    glNewList(index, GL_COMPILE);
        glBegin(GL_TRIANGLES);

        //top of the board
        glNormal3d(0.0, 1.0, 0.0);
        glTexCoord2d(0, 0);
        glVertex3d(-board.width/2.0, 0, -board.height / 2.0 );
        glTexCoord2d(1,0);
        glVertex3d(-board.width/2.0, 0, board.height / 2.0 );
        glTexCoord2d(1,1);
        glVertex3d(board.width/2.0, 0, board.height / 2.0 );

        glTexCoord2d(0,1);
        glVertex3d(board.width/2.0, 0, -board.height / 2.0 );
        glTexCoord2d(0,0);
        glVertex3d(-1 * board.width/2.0, 0, -1 * board.height / 2.0 );
        glTexCoord2d(1,1);
        glVertex3d(board.width/2.0, 0, board.height / 2.0 );

        //top corner
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2d(1, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, -board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, -board.height / 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, -board.height / 2.0 );

        //right corner
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, -board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, board.height / 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, board.height / 2.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, board.height / 2.0 );

        //bottom corner
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );
        glTexCoord2d(1, WALL_THICKNESS / 10.0);
        glVertex3d(board.width/2.0, 0, board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, board.height / 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, board.height / 2.0);
        glTexCoord2d(1, 0);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, board.height / 2.0 );

        //left corner
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2d(1, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );
        glTexCoord2d(1, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, -board.height / 2.0 );

        glTexCoord2d(0, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1, 0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );
        glTexCoord2d(0, WALL_THICKNESS / 10.0);
        glVertex3d(-board.width/2.0, 0, -board.height / 2.0 );

        //bottom of the board
        glNormal3d(0.0, -1.0, 0.0);
        glTexCoord2d(0,0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(0,1);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1,0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );

        glTexCoord2d(0,1);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, -board.height / 2.0 );
        glTexCoord2d(1,1);
        glVertex3d(board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );
        glTexCoord2d(1,0);
        glVertex3d(-board.width/2.0, -WALL_THICKNESS, board.height / 2.0 );

        glEnd();

    glEndList();
    return index;
}

